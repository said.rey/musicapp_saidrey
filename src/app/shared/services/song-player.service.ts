import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SongPlayerService {

  callback: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }
}
