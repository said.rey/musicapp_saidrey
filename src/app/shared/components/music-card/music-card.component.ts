import { SongPlayerService } from './../../services/song-player.service';
import { SongModel } from './../../../core/models/songs.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-music-card',
  templateUrl: './music-card.component.html',
  styleUrls: ['./music-card.component.css']
})
export class MusicCardComponent implements OnInit {
  @Input() mode: 'small' | 'big' = 'small'
  @Input() song: SongModel = { _id:0, name:'', album:'', url:'', cover:'' };

  constructor(private songPlayerService: SongPlayerService) { }

  ngOnInit(): void {
  }

  sendSongToPlay(song: SongModel): void {
    this.songPlayerService.callback.emit(song);
  }

}
