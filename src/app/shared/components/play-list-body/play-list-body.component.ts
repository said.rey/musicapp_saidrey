import { SongModel } from '@core/models/songs.model';
import { Component, OnInit } from '@angular/core';
import * as dataRaw from '../../../data/songs.json';

@Component({
  selector: 'app-play-list-body',
  templateUrl: './play-list-body.component.html',
  styleUrls: ['./play-list-body.component.css']
})
export class PlayListBodyComponent implements OnInit {
  songs: SongModel[] = [];
  sortValues: { column: string | null, orderBy: string} = {column:null, orderBy:'asc'};
  constructor() { }

  ngOnInit(): void {
    const { data } = (dataRaw as any).default;
    this.songs = data;
  }

  changeSort(column: string): void {
    const order = this.sortValues.orderBy;
    this.sortValues = {
      column:column,
      orderBy: order === 'asc' ? 'desc' : 'asc'
    }
  }

}
