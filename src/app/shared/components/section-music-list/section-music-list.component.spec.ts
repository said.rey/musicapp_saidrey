import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionMusicListComponent } from './section-music-list.component';

describe('SectionMusicListComponent', () => {
  let component: SectionMusicListComponent;
  let fixture: ComponentFixture<SectionMusicListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionMusicListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionMusicListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
