import { Component, Input, OnInit } from '@angular/core';
import { SongModel } from '@core/models/songs.model';

@Component({
  selector: 'app-section-music-list',
  templateUrl: './section-music-list.component.html',
  styleUrls: ['./section-music-list.component.css']
})
export class SectionMusicListComponent implements OnInit {
  @Input() title: string=''
  @Input() mode: 'small' | 'big' = 'big'
  @Input() dataSongs: Array<SongModel> = [] 

  constructor() { }

  ngOnInit(): void {
  }

}
