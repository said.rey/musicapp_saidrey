import { Subscription } from 'rxjs';
import { SongModel } from './../../../core/models/songs.model';
import { SongPlayerService } from './../../services/song-player.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.css']
})
export class BottomBarComponent implements OnInit, OnDestroy {

  listObservers$: Array<Subscription> = []; 

  constructor(private songPlayerService: SongPlayerService) { }
  
  ngOnInit(): void {
    const observerToPlay$: Subscription = this.songPlayerService.callback.subscribe(
      (response: SongModel) => {
        console.log('Recibiendo cancion....', response);
      }
    )

    this.listObservers$ = [observerToPlay$];  

  }

  ngOnDestroy(): void {
    this.listObservers$.forEach(element => {
      element.unsubscribe()
    });
  }

}
