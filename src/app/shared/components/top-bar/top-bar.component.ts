import { SideBarService } from './../../services/side-bar.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { SideBarComponent } from '../side-bar/side-bar.component';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  constructor(
    private sideBarService : SideBarService
  ) { }

  cambiar() {
    this.sideBarService.toggle();
  }

  ngOnInit(): void {
  }

}
