import { SideBarComponent } from '@shared/components/side-bar/side-bar.component';
import { BottomBarComponent } from '@shared/components/bottom-bar/bottom-bar.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionMusicListComponent } from '@shared/components/section-music-list/section-music-list.component';
import { MusicCardComponent } from './components/music-card/music-card.component';
import { PlayListHeaderComponent } from './components/play-list-header/play-list-header.component';
import { PlayListBodyComponent } from './components/play-list-body/play-list-body.component';
import { OrderListPipe } from './pipes/order-list.pipe';
import { WithoutImageDirective } from './directives/without-image.directive';



@NgModule({
  declarations: [
    SideBarComponent,
    BottomBarComponent,
    SectionMusicListComponent,
    MusicCardComponent,
    PlayListHeaderComponent,
    PlayListBodyComponent,
    OrderListPipe,
    WithoutImageDirective,

  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    SideBarComponent,
    BottomBarComponent,
    SectionMusicListComponent,
    MusicCardComponent,
    PlayListHeaderComponent,
    PlayListBodyComponent,
    RouterModule,
    OrderListPipe,
    WithoutImageDirective,
  ]
})
export class SharedModule { }
