import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appWithoutImage]'
})
export class WithoutImageDirective {
  @HostListener('error') handleError(): void {
    const elementNative = this.elementHost.nativeElement;
    elementNative.src = '../../../assets/images/without-image.jpg'
  }

  constructor(private elementHost: ElementRef) { }

}
