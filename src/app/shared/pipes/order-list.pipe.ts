import { SongModel } from './../../core/models/songs.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderList'
})
export class OrderListPipe implements PipeTransform {

  transform(value: Array<any>, column: string | null = null, sortBy: string = 'asc'): SongModel[] {
    if (column === null) {
      return value
    } else {
      const tmpList = value.sort((a, b) => {
        if (a[column] < b[column]) {
          return -1
        }
        else if (a[column] === b[column]) {
          return 0;
        }
        else if (a[column] > b[column]) {
          return 1;
        }
        return 1
      });

      return (sortBy === 'asc') ? tmpList : tmpList.reverse()
    }
  }

}
