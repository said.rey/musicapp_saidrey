import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SongsRoutingModule } from './songs-routing.module';
import { SongsPageComponent } from './pages/songs-page/songs-page.component';


@NgModule({
  declarations: [
    SongsPageComponent
  ],
  imports: [
    CommonModule,
    SongsRoutingModule,
    SharedModule
  ]
})
export class SongsModule { }
