import { SongService } from './../../services/song.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SongModel } from '@core/models/songs.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-songs-page',
  templateUrl: './songs-page.component.html',
  styleUrls: ['./songs-page.component.css']
})
export class SongsPageComponent implements OnInit, OnDestroy {

  songsTrending: Array<SongModel> = [];
  songsRandom: Array<SongModel> = [];

  listObservers$: Array<Subscription> = []

  constructor(private songService: SongService) { }

  ngOnInit(): void {
    const observerTrending$ = this.songService.dataSongsTrending$
    .subscribe(
      response => {
        this.songsTrending = response;
        this.songsRandom = response;
      }
    )
    this.listObservers$ = [observerTrending$];
  }

  ngOnDestroy(): void {
    this.listObservers$.forEach(s => s.unsubscribe)
  }

}
