import { SongModel } from './../../../core/models/songs.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as dataRaw from '../../../data/songs.json';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  dataSongsTrending$: Observable<SongModel[]> = of([]);

  constructor() { 
    const { data }: any = (dataRaw as any).default;
    this.dataSongsTrending$ = of(data);
  }
}
