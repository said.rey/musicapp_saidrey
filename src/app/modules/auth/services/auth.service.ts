import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly URL = environment.url_api_token;
  private readonly ID = environment.client_ID;
  private readonly SECRET = environment.client_secret;

  constructor(private httpClient: HttpClient) { }

  sendCredentials(email: string, password: string): Observable<any>{
    const body = {
      email,
      password
    }
  
    return this.httpClient.post(`${this.URL}/auth/login`, body);
  }

  getToken(): Observable<any>{
    const httpParams = new HttpParams().append("grant_type", "client_credentials");
    
    const headers = { 
      'Authorization': 'Basic ' + btoa(this.ID+':'+this.SECRET),
      'Content-Type':'application/x-www-form-urlencoded'
    };
    
    return this.httpClient.post(`${this.URL}`, httpParams.toString(), { 'headers': headers })
  }
  
}
